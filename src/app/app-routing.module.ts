import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './theme/layout/admin/admin.component';
import {CoreChartModule} from './demo/pages/core-chart/core-chart.module';
import {AuthComponent} from './theme/layout/auth/auth.component';
import { MorauthenticationComponent } from './morauthentication/morauthentication.component';
import { MordashboardComponent } from './moradmindashboard/morlayout/mordashboard/mordashboard.component';
import { MoradminComponent } from './moradmindashboard/morlayout/mordashboard/moradmin/moradmin.component';
import { UserModule } from './user/user.module';
import { UserComponent } from './user/user.component';

const routes: Routes = [
  {
    path: 'admin',
    component: MordashboardComponent,
    children: [
      {
        path: 'dashboard',
        component: MoradminComponent,
      },
      {
        path: 'user',
        loadChildren: () => import('./user/user.module')
        .then(module => module.UserModule)
      },
      {
        path: 'machine',
        loadChildren: () => import('./machine/machine.module')
        .then(module => module.MachineModule)
      },
      {
        path: 'basic',
        loadChildren: () =>
          import('./demo/ui-elements/ui-basic/ui-basic.module')
          .then(module => module.UiBasicModule)
      },
      {
        path: 'forms',
        loadChildren: () =>
          import('./demo/pages/form-elements/form-elements.module')
          .then(module => module.FormElementsModule)
      },
      {
        path: 'tbl-bootstrap',
        loadChildren: () => 
        import('./demo/pages/tables/tbl-bootstrap/tbl-bootstrap.module')
        .then(module => module.TblBootstrapModule)
      },
      {
        path: 'charts',
        loadChildren: () => import('./demo/pages/core-chart/core-chart.module')
        .then(module => module.CoreChartModule)
      },
      {
        path: 'maps',
        loadChildren: () => import('./demo/pages/core-maps/core-maps.module')
        .then(module => module.CoreMapsModule)
      },
      {
        path: 'sample-page',
        loadChildren: () => 
        import('./demo/pages/sample-page/sample-page.module')
        .then(module => module.SamplePageModule)
      },
      ]
},
   
  {
    path: '',
    component: MorauthenticationComponent,
    children: [
      {
        path: '',
        redirectTo: 'auth/login',
        pathMatch: 'full'
      },
      {
        path: 'auth',
        loadChildren: () => 
        import('./morauthentication/morauthentication.module')
        .then(module => module.MorauthenticationModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
