export enum ContractStatusEnum {
  
  Active = 1 ,
	OwnerRevised = 2,
	HirerRevised= 3 ,
	Accepted = 4,
	Rejected = 5,
	Pending_In_Cart = 6,
	Deleted_from_Cart = 7 

}