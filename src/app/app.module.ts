import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './theme/shared/shared.module';
import { AppComponent } from './app.component';
import { AdminComponent } from './theme/layout/admin/admin.component';
import { AuthComponent } from './theme/layout/auth/auth.component';
import { NavigationComponent } from './theme/layout/admin/navigation/navigation.component';
import { NavContentComponent } from './theme/layout/admin/navigation/nav-content/nav-content.component';
import { NavGroupComponent } from './theme/layout/admin/navigation/nav-content/nav-group/nav-group.component';
import { NavCollapseComponent } from './theme/layout/admin/navigation/nav-content/nav-collapse/nav-collapse.component';
import { NavItemComponent } from './theme/layout/admin/navigation/nav-content/nav-item/nav-item.component';
import { NavBarComponent } from './theme/layout/admin/nav-bar/nav-bar.component';
import { NavLeftComponent } from './theme/layout/admin/nav-bar/nav-left/nav-left.component';
import { NavSearchComponent } from './theme/layout/admin/nav-bar/nav-left/nav-search/nav-search.component';
import { NavRightComponent } from './theme/layout/admin/nav-bar/nav-right/nav-right.component';
import {ChatUserListComponent} from './theme/layout/admin/nav-bar/nav-right/chat-user-list/chat-user-list.component';
import {FriendComponent} from './theme/layout/admin/nav-bar/nav-right/chat-user-list/friend/friend.component';
import {ChatMsgComponent} from './theme/layout/admin/nav-bar/nav-right/chat-msg/chat-msg.component';
import { ConfigurationComponent } from './theme/layout/admin/configuration/configuration.component';

import { ToggleFullScreenDirective } from './theme/shared/full-screen/toggle-full-screen';

/* Menu Items */
import { NavigationItem } from './theme/layout/admin/navigation/navigation';
import { NgbButtonsModule, NgbDropdownModule, NgbTabsetModule, 
  NgbTooltipModule, NgbPagination, NgbPaginationModule, NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { MorauthenticationComponent } from './morauthentication/morauthentication.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './service/auth.service';
import { OauthService } from './service/oauth.service';
import { ToastrModule } from 'ngx-toastr';
import { UserComponent } from './user/user.component';
import { MordashboardComponent } from './moradmindashboard/morlayout/mordashboard/mordashboard.component';
import { NavbarComponent } from './moradmindashboard/morlayout/mordashboard/navbar/navbar.component';
import { MorconfigurationComponent } from './moradmindashboard/morlayout/mordashboard/morconfiguration/morconfiguration.component';
import { MornavigationComponent } from './moradmindashboard/morlayout/mordashboard/mornavigation/mornavigation.component';
import { MornavcontentComponent } from './moradmindashboard/morlayout/mordashboard/mornavigation/mornavcontent/mornavcontent.component';
import { MornavCollapseComponent } from './moradmindashboard/morlayout/mordashboard/mornavigation/mornavcontent/mornav-collapse/mornav-collapse.component';
import { MornavGroupComponent } from './moradmindashboard/morlayout/mordashboard/mornavigation/mornavcontent/mornav-group/mornav-group.component';
import { MornavItemComponent } from './moradmindashboard/morlayout/mordashboard/mornavigation/mornavcontent/mornav-item/mornav-item.component';
import { MorNavigationItem } from './moradmindashboard/morlayout/mordashboard/mornavigation/mornavigation';
import { MordashboardModule } from './moradmindashboard/morlayout/mordashboard/mordashboard.module';
import { MoradminComponent } from './moradmindashboard/morlayout/mordashboard/moradmin/moradmin.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { UserModule } from './user/user.module';
import { ModalModule } from 'ngx-bootstrap/modal';



@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    AuthComponent,
    NavigationComponent,
    NavContentComponent,
    NavGroupComponent,
    NavCollapseComponent,
    NavItemComponent,
    NavBarComponent,
    NavLeftComponent,
    NavSearchComponent,
    NavRightComponent,
    ChatUserListComponent,
    FriendComponent,
    ChatMsgComponent,
    ConfigurationComponent,
    ToggleFullScreenDirective,
    MorauthenticationComponent,
    MordashboardComponent,
    NavbarComponent,
    MorconfigurationComponent,
    MornavigationComponent,
    MornavcontentComponent,
    MornavCollapseComponent,
    MornavGroupComponent,
    MornavItemComponent,
    MoradminComponent
   
  ],
  imports: [
    BrowserModule,
    NgbModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    NgbDropdownModule,
    NgbTooltipModule,
    NgbButtonsModule,
    NgbTabsetModule,
    HttpModule,
    MordashboardModule,
    UserModule,
    // required to ope bsmodal (bootstrap)
    ModalModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 3500,
      positionClass: 'toast-top-center',
      preventDuplicates: true,
}),
    
  ],
  providers: [NavigationItem,MorNavigationItem,
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    OauthService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
