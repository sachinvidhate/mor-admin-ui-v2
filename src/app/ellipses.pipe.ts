import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ellipses'
})
export class EllipsesPipe implements PipeTransform {

  transform(val, args) {
    if (args === undefined) {
      return val;
    }
    
      if(val!=null){
    if (val.length > args) {
      return val.substring(0, args) + '...';
    } else {
      return val;
    }
  }
  }
}
