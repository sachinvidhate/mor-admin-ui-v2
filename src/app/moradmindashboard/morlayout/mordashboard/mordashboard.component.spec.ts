import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MordashboardComponent } from './mordashboard.component';

describe('MordashboardComponent', () => {
  let component: MordashboardComponent;
  let fixture: ComponentFixture<MordashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MordashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MordashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
