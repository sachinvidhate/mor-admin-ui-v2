import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoradminComponent } from './moradmin.component';

describe('MoradminComponent', () => {
  let component: MoradminComponent;
  let fixture: ComponentFixture<MoradminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoradminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoradminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
