import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoradminComponent } from './moradmin.component';
import { Routes, RouterModule } from '@angular/router';



const routes: Routes = [
  {
    path: '',
    component: MoradminComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MoradminRoutingModule { }
