import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoradminComponent } from './moradmin.component';
import { MoradminRoutingModule } from './moradmin-routing.module';



@NgModule({
  declarations: [
    MoradminComponent
  ],
  imports: [
    CommonModule,
    MoradminRoutingModule
  ]
})
export class MoradminModule { }
