import { Component, OnInit, Input, NgZone } from '@angular/core';
import { MorNavigationItem } from '../../mornavigation';
import {Location} from '@angular/common';

@Component({
  selector: 'app-mornav-group',
  templateUrl: './mornav-group.component.html',
  styleUrls: ['./mornav-group.component.scss']
})
export class MornavGroupComponent implements OnInit {
  @Input() item: MorNavigationItem;
  @Input() layout1: boolean = false;
  @Input() activeId: any;
  public gradientConfig: any;
  constructor(private zone: NgZone, private location: Location) { }

  ngOnInit() {
    let current_url = this.location.path();
    if (this.location['_baseHref']) {
      current_url = this.location['_baseHref'] + this.location.path();
    }
    const link = "a.nav-link[ href='" + current_url + "' ]";
    const ele = document.querySelector(link);
    if (ele !== null && ele !== undefined) {
      const parent = ele.parentElement;
      const up_parent = parent.parentElement.parentElement;
      const last_parent = up_parent.parentElement;
      if (parent.classList.contains('pcoded-hasmenu')) {
        if (this.gradientConfig['layout'] === 'vertical') {
          parent.classList.add('pcoded-trigger');
        }
        parent.classList.add('active');
      } else if(up_parent.classList.contains('pcoded-hasmenu')) {
        if (this.gradientConfig['layout'] === 'vertical') {
          up_parent.classList.add('pcoded-trigger');
        }
        up_parent.classList.add('active');
      } else if (last_parent.classList.contains('pcoded-hasmenu')) {
        if (this.gradientConfig['layout'] === 'vertical') {
          last_parent.classList.add('pcoded-trigger');
        }
        last_parent.classList.add('active');
      }
    }

  }

}
