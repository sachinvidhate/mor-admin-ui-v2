import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MornavGroupComponent } from './mornav-group.component';

describe('MornavGroupComponent', () => {
  let component: MornavGroupComponent;
  let fixture: ComponentFixture<MornavGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MornavGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MornavGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
