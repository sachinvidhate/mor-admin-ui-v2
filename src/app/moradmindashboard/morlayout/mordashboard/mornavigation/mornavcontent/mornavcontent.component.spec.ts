import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MornavcontentComponent } from './mornavcontent.component';

describe('MornavcontentComponent', () => {
  let component: MornavcontentComponent;
  let fixture: ComponentFixture<MornavcontentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MornavcontentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MornavcontentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
