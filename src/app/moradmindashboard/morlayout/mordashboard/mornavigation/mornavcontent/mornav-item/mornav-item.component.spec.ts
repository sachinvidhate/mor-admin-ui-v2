import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MornavItemComponent } from './mornav-item.component';

describe('MornavItemComponent', () => {
  let component: MornavItemComponent;
  let fixture: ComponentFixture<MornavItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MornavItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MornavItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
