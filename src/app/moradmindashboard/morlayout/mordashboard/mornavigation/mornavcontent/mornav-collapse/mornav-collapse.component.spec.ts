import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MornavCollapseComponent } from './mornav-collapse.component';

describe('MornavCollapseComponent', () => {
  let component: MornavCollapseComponent;
  let fixture: ComponentFixture<MornavCollapseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MornavCollapseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MornavCollapseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
