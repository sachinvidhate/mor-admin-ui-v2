import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MornavigationComponent } from './mornavigation.component';

describe('MornavigationComponent', () => {
  let component: MornavigationComponent;
  let fixture: ComponentFixture<MornavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MornavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MornavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
