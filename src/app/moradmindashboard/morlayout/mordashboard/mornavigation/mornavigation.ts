import {Injectable} from '@angular/core';
import { MachineTypeEnum } from 'src/app/enum/machinetypeenum.enum';

export interface MorNavigationItem {
  id: string;
  title: string;
  type: 'item' | 'collapse' | 'group';
  translate?: string;
  icon?: string;
  hidden?: boolean;
  url?: string;
  classes?: string;
  exactMatch?: boolean;
  external?: boolean;
  target?: boolean;
  breadcrumbs?: boolean;
  function?: any;
  badge?: {
    title?: string;
    type?: string;
  };
  children?: MorNavigation[];
}

export interface MorNavigation extends MorNavigationItem {
  children?: MorNavigationItem[];
}

const MorNavigationItems = [
  {
    id: 'navigation',
    title: 'Navigation',
    type: 'group',
    icon: 'feather icon-monitor',
    children: [
      {
        id: 'dashboard',
        title: 'Dashboard',
        type: 'item',
        icon: 'feather icon-home',
        url: 'dashboard',
        breadcrumbs: false
      },
      {
        id: 'user-layouts',
        title: 'User',
        type: 'collapse',
        icon: 'feather icon-user',
        
        children: [
          {
            title: 'List of User',
            type: 'item',
            url: 'admin/user'
          },
          {
            title: 'List of Owner',
            type: 'item',
            url: 'admin/user/list/owner'
          },
          {
           title: 'Update User',
            type: 'item',
            url: 'user/updateuser',
            
          },
        ]
      },
      {
        id: 'machine-layouts',
        title: 'Machine',
        type: 'collapse',
        icon: 'feather icon-list',
        children: [
          {
           title: 'Pending for Documentation',
            type: 'item',
            url: 'admin/machine/machinestatus/'+MachineTypeEnum.DOC_PENDING,
            
          },
          {
            title: 'Pending for GPS Installation',
            type: 'item',
            url: 'admin/machine/machinestatus/'+MachineTypeEnum.PENDING_GPS,
          },
          {
            title: 'Pending for Rates',
            type: 'item',
            url: 'admin/machine/machinestatus/'+MachineTypeEnum.RATE_PENDING,
          
          },
          {
            title: 'Ready to go',
            type: 'item',
            url: 'admin/machine/machinestatus/'+MachineTypeEnum.IDLE,
          
          },
          {
            title: 'On Rent',
            type: 'item',
            url: 'admin/machine/machinestatus/'+MachineTypeEnum.ON_RENT,
          
          }

        ]
      },
      {
        id: 'dashboard',
        title: 'Contracts',
        type: 'item',
        icon: 'feather icon-file-text',
        url: 'dashboard',
        breadcrumbs: false
      },
      {
        id: 'dashboard',
        title: 'Ignition request',
        type: 'item',
        icon: 'feather icon-home',
        url: 'dashboard',
        breadcrumbs: false
      },
      {
        id: 'dashboard',
        title: 'GPS Device',
        type: 'item',
        icon: 'feather icon-home',
        url: 'dashboard',
        breadcrumbs: false
      },
      {
        id: 'dashboard',
        title: 'Bulk Request',
        type: 'item',
        icon: 'feather icon-home',
        url: 'dashboard',
        breadcrumbs: false
      },
    ]
  }
];

@Injectable()
export class MorNavigationItem {
  public get() {
    return MorNavigationItems;
  }
}
