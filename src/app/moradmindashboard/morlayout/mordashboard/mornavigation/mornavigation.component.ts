import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { GradientConfig } from 'src/app/app-config';


@Component({
  selector: 'app-mornavigation',
  templateUrl: './mornavigation.component.html',
  styleUrls: ['./mornavigation.component.scss']
})
export class MornavigationComponent implements OnInit {
  public windowWidth: number;
  public gradientConfig: any;
  @Output() onNavMobCollapse = new EventEmitter();
  constructor() {
    this.gradientConfig = GradientConfig.config;
    this.windowWidth = window.innerWidth;
   }

  ngOnInit() {
  }

  navMobCollapse() {
    if (this.windowWidth < 992) {
      this.onNavMobCollapse.emit();
    }
  }

}
