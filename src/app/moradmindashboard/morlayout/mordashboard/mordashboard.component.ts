import { Component, OnInit, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { GradientConfig } from 'src/app/app-config';
import { OauthService } from 'src/app/service/oauth.service';

@Component({
  selector: 'app-mordashboard',
  templateUrl: './mordashboard.component.html',
  styleUrls: ['./mordashboard.component.scss']
})
export class MordashboardComponent implements OnInit {
  public gradientConfig: any; 
  public navCollapsed: boolean;
  public navCollapsedMob: boolean;
  public windowWidth: number;
  constructor(private zone: NgZone, private location: Location,
    public oauth:OauthService) {
    this.gradientConfig = GradientConfig.config;
    let currentURL = this.location.path();
    const baseHerf = this.location['_baseHref'];
    if (baseHerf) {
      currentURL = baseHerf + this.location.path();
    }

    this.windowWidth = window.innerWidth;

    if (currentURL === baseHerf + '/layout/collapse-menu'
      || currentURL === baseHerf + '/layout/box'
      || (this.windowWidth >= 992 && this.windowWidth <= 1024)) {
      this.gradientConfig.collapseMenu = true;
    }

    this.navCollapsed = (this.windowWidth >= 992) 
    ? this.gradientConfig.collapseMenu : false;
    this.navCollapsedMob = false;

   }

  ngOnInit() {
    if(localStorage.getItem('options')!=undefined &&
     localStorage.getItem('options')!=null){
      this.oauth.options=JSON.parse(localStorage.getItem('options'));
    }
    if (this.windowWidth < 992) {
      this.gradientConfig.layout = 'vertical';
      setTimeout(() => {
        document.querySelector('.pcoded-navbar').
        classList.add('menupos-static');
        (document.querySelector('#nav-ps-gradient-able') as HTMLElement)
        .style.maxHeight = '100%';
      }, 500);
    }
  }

  navMobClick() {
    if (this.windowWidth < 992) {
      if (this.navCollapsedMob && 
        !(document.querySelector('app-mornavigation.pcoded-navbar')
        .classList.contains('mob-open'))) {
        this.navCollapsedMob = !this.navCollapsedMob;
        setTimeout(() => {
          this.navCollapsedMob = !this.navCollapsedMob;
        }, 100);
      } else {
        this.navCollapsedMob = !this.navCollapsedMob;
      }
    }
  }

  /* initializeComponents(){
    this.oauth.obtainAccessToken().subscribe(
      (response:Response) => {
        const data=response.json();
  
         this._service.saveToken(data);
         
   
        },
      (error) => console.log(error)
    )
  
  } */

}
