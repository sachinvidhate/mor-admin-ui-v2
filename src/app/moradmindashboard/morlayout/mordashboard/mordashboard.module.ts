import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MordashboardComponent } from './mordashboard.component';
import { MordashboardRoutingModule } from './mordashboard-routing.module';



@NgModule({
  declarations: [
    
  ],
  imports: [
    CommonModule,
    MordashboardRoutingModule
  ]
})
export class MordashboardModule { }
