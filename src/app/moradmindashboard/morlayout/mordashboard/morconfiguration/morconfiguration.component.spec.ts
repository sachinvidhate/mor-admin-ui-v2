import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MorconfigurationComponent } from './morconfiguration.component';

describe('MorconfigurationComponent', () => {
  let component: MorconfigurationComponent;
  let fixture: ComponentFixture<MorconfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MorconfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MorconfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
