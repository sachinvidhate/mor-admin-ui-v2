import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OauthService } from 'src/app/service/oauth.service';
import { ToastrService } from 'ngx-toastr';
import { Response } from '@angular/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(public router: Router, public toastr:ToastrService, 
    public oAuthService:OauthService) { }
  public email:string;
  public password:string;

  ngOnInit() {
  }

  
  loginAction(){
   
    this.oAuthService.obtainAccessToken(this.email,this.password).subscribe(
        (response:Response) => {
            const data=response.json();
            this.oAuthService.saveToken(data);
            localStorage.setItem('isLoggedin', 'true');
            localStorage.setItem('loggedInEmail', this.email);
            this.router.navigate(['/admin/dashboard']);
        },
        (error) => {
            this.showError("Invalid username or password found");
            console.log(error);
        })
  }

  showSuccess(msg:string) {
    this.toastr.success(msg,'Success');
  }
  showError(msg:string) {
      this.toastr.error( msg, 'Error');
  }
}
