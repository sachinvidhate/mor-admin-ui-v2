import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MorauthenticationComponent } from './morauthentication.component';
import { MorauthenticationRoutingModule } from './morauthentication-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    MorauthenticationRoutingModule
  ]
})
export class MorauthenticationModule { }
