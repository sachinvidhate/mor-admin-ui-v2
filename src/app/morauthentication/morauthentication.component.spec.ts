import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MorauthenticationComponent } from './morauthentication.component';

describe('MorauthenticationComponent', () => {
  let component: MorauthenticationComponent;
  let fixture: ComponentFixture<MorauthenticationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MorauthenticationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MorauthenticationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
