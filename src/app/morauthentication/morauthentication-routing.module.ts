import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MorauthenticationComponent } from './morauthentication.component';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'login',
        loadChildren: () => import('./login/login.module')
        .then(module => module.LoginModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MorauthenticationRoutingModule { }
