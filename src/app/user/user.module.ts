import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
import { UpdateuserComponent } from './updateuser/updateuser.component';
import { UserRoutingModule } from './user-routing.module';
import { NgbPagination, NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { EllipsesPipe } from '../ellipses.pipe';
import { CardModule } from '../theme/shared/components';
import { FormsModule } from '@angular/forms';
import { UserdetailsComponent } from './userdetails/userdetails.component';


@NgModule({
  declarations: [
   UpdateuserComponent,
   UserComponent,
   EllipsesPipe,
   UserdetailsComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    NgxDatatableModule,
    CardModule,
    FormsModule
  ]
})
export class UserModule { }
