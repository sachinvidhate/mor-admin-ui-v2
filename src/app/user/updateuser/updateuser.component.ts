import { Component, OnInit } from '@angular/core';
import { Params, Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';
import { User } from 'src/app/model/users.model';
import { ToastrService } from 'ngx-toastr';
import {Location} from '@angular/common';
@Component({
  selector: 'app-updateuser',
  templateUrl: './updateuser.component.html',
  styleUrls: ['./updateuser.component.scss']
})
export class UpdateuserComponent implements OnInit {
  public user= new User();
  public accName:String;
  public accNo:String;
  public ifsc:String;
  public branch:String;
  public upi:String;
  public userId:any;
  public isLoading : Boolean = false;
  constructor(public router:Router,public userService:UserService,
    private route: ActivatedRoute,public toastr:ToastrService,
    private _location: Location) { }

  ngOnInit() {

    this.route.params.forEach((params: Params) => {
      let id = params['id']; 
      this.userId=id;
      this.getUserById(id);
    });
  }

  getUserById(id:string){
    this.userService.getUserById(id).subscribe(
      (response:any) => {
        this.user = response.json();
        this.accName=response.json().accounts[0].accName;
        this.accNo=response.json().accounts[0].accNo;
        this.branch=response.json().accounts[0].branch;
        this.ifsc=response.json().accounts[0].ifsc;
        this.upi=response.json().accounts[0].upi;
      },
      (error) => {
        console.log(error);
      }
    )
  }

  updateUser(){
    //this.isLoading = true;
    this.user.id=this.userId;
    this.user.accName=this.accName;
    this.user.accNo=this.accNo;
    this.user.branch=this.branch;
    this.user.ifsc=this.ifsc;
    this.user.upi=this.upi;
      this.userService.updateUser(this.user).subscribe(
            (response:any) => {
              //this.isLoading=false;
              this.showSuccess("User Updated Succefully"); 
              this.toastr.success('User updated succefully', 'Congrats');
            },
            (error) => {
              this.isLoading=false;
            console.log(error.json());
            if(error && error.json() && 
            (error.json().status != 500 && error.json().status != 404)){
               this.toastr.error(error.json().message,'error'); 
            } else {
              this.toastr.error('Some error while updating the user','oops!!!'); 
            }
          }
      )
  }
  showSuccess(msg:string) {
    this.toastr.success('User updated succefully', 'Congrats');
    this._location.back();
  }

  cancel(){
    this._location.back();
//    this.router.navigateByUrl('home');
  }

}
