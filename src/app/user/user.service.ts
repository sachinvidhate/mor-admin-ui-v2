import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment'; 
import { AuthService } from '../service/auth.service';
import { OauthService } from '../service/oauth.service';
import { User } from '../model/users.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
 
  constructor(private http:Http,private httpClient: HttpClient,
    private auth: OauthService) { 
    }

  getUserList(searchText:string,pageNumber:number,size:number){
    return this.http.get(environment.servicesURL+
      'api/v1/admin/users'+'?searchText='+searchText+
      '&page='+pageNumber+'&size='+size,this.auth.options);
  }

  getOwnerList(searchText:string,pageNumber:number,size:number){
    return this.http.get(environment.servicesURL+'api/v1/admin/owners'+'?searchText='
    +searchText+'&page='+pageNumber+'&size='+size,this.auth.options);
  }

  getUserById(id:string){
    return this.http.get(environment.servicesURL+'api/v1/user/'+id,this.auth.options);
  }

  updateUser(updateUser:User){ 
    return this.http.put(environment.servicesURL+'/api/v1/admin/users'
    ,updateUser,this.auth.options);
  }
}                       
