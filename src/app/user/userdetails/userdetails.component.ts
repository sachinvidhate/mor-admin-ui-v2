import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';
import { User } from 'src/app/model/users.model';

@Component({
  selector: 'app-userdetails',
  templateUrl: './userdetails.component.html',
  styleUrls: ['./userdetails.component.scss']
})
export class UserdetailsComponent implements OnInit {

  public userId;
  public user= new User();
  public accName:String;
  public accNo:String;
  public ifsc:String;
  public branch:String;
  public upi:String;
  constructor(public router:Router,public ownerService:UserService,
    private route: ActivatedRoute,private toastr: ToastrService) { }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      let id = params['id']; 
      console.log("id",id);
      this.userId=id;
      this.getUserById(id);
    });
  }
  getUserById(id:string){
    //this.spinnerService.show();
    this.ownerService.getUserById(id).subscribe(
      (response:any) => {
        this.user = response.json();
        console.log(this.user,"user");
        if(response.json().accounts.length!=0){
        this.accName=response.json().accounts[0].accName;
        this.accNo=response.json().accounts[0].accNo;
        this.branch=response.json().accounts[0].branch;
        this.ifsc=response.json().accounts[0].ifsc;
        this.upi=response.json().accounts[0].upi;
      }
        //this.panImageId=this.user.panImageId;
        //this.spinnerService.hide();
      },
      
      (error) => { 
        //this.spinnerService.hide(); 
        console.log(error);
      }
    )
          
  }

}
