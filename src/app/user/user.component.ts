import { Component, OnInit, ElementRef, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from './user.service';
import { User } from '../model/users.model';
import { AuthService } from '../service/auth.service';
import { OauthService } from '../service/oauth.service';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { AddComments } from '../model/addcomments.model';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
 /*  public users:User[]=new Array(); */
 
  public searchText:string=""; 
  public role:any;
  readonly headerHeight = 50;
  readonly rowHeight = 50;
  readonly pageLimit = 1;
  isLoading: boolean;
  rows: User[] = [];
  public cols = [];
  public comments : any;
  ColumnMode = ColumnMode;
  public addcomments = new AddComments;
  public displayComments: boolean = false;
  public modalRef: BsModalRef;
  public userId=new User();

  constructor(public router:Router,public userService:UserService,
    private route: ActivatedRoute,private el: ElementRef,
    private modalService: BsModalService) { }

  
  ngOnInit() {
   
    this.route.params.forEach((params: Params) => {
      this.role= params['role'];  
    });
    this.onScroll(0);

  }

  onScroll(offsetY: number) {
    // total height of all rows in the viewport
    const viewHeight = this.el.nativeElement.getBoundingClientRect().height
     - this.headerHeight;
     console.log("viewheight",this.el.nativeElement.getBoundingClientRect().height);
    //check if we scrolled to the end of the viewport
    if (!this.isLoading && offsetY + viewHeight >= this.rows.length * this.rowHeight) {
      // total number of results to load
      let limit = this.pageLimit;
      // check if we haven't fetched any results yet
      if (this.rows.length === 0) {
        // calculate the number of rows that fit within viewport
        const pageSize = Math.ceil(viewHeight / this.rowHeight);
        // change the limit to pageSize such that we fill the first page entirely
        // (otherwise, we won't be able to scroll past it)
        limit = Math.max(pageSize, this.pageLimit);
      }
      console.log("role",this.role);
      if(this.role == "owner"){
        this.loadOwnerPage(limit);
      }else{
        this.loadUserPage(limit);
      }
      
    }
  }
  private loadUserPage(limit: number) {
    // set the loading flag, which serves two purposes:
    // 1) it prevents the same page from being loaded twice
    // 2) it enables display of the loading indicator
    this.isLoading = true;
    this.userService.getUserList(this.searchText,this.rows.length, limit)
    .subscribe(response => {
      const rows = [...this.rows, ...response.json().content];
      this.rows = rows;
      this.isLoading = false;
    });
  }

  private loadOwnerPage(limit: number) {
    // set the loading flag, which serves two purposes:
    // 1) it prevents the same page from being loaded twice
    // 2) it enables display of the loading indicator
    console.log("owner page");
    this.isLoading = true;
    this.userService.getOwnerList(this.searchText,this.rows.length, limit)
    .subscribe(response => {
      const rows = [...this.rows, ...response.json().content];
      this.rows = rows;
      this.isLoading = false;
    });
  }

 /*  showCommentDialog(userData) {
    this.comments=userData.comments;
    this.addcomments.id=userData.id;
    this.displayComments = true;
    console.log("comments", this.comments);
  } */
  
  openModal(viewComment: TemplateRef<any>,selectedUser: any) {
    /* this.comments=selectedUser.comments;
    this.addcomments.id=selectedUser.id; */
    this.modalRef = this.modalService.show(viewComment,{show:true});
    
    console.log("comments",this.comments);
  } 

  machineList(owner){
    //this.spinnerService.show();
    this.userId.id=owner.id;
    localStorage.setItem("ownerName",owner.fullName); 
    //this.spinnerService.show();
    this.router.navigateByUrl('admin/machine/machinelist/'+this.userId.id);
   }
}
