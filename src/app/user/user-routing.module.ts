import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
import { Routes, RouterModule } from '@angular/router';
import { UpdateuserComponent } from './updateuser/updateuser.component';
import { UserdetailsComponent } from './userdetails/userdetails.component';


const routes: Routes = [
  {
    path: '',
    children: [
      { 
        path: '', 
        component: UserComponent 
      },
      { 
        path: 'list/:role', 
        component: UserComponent 
      },
      {
        path: 'updateuser/:id',
        component:UpdateuserComponent
      },
      {
        path: 'userdetails/:id',
        component:UserdetailsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
