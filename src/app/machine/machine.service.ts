import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { OauthService } from '../service/oauth.service';
import { environment } from '../../environments/environment'; 
import { Machine } from '../model/machine.model';

@Injectable({
  providedIn: 'root'
})
export class MachineService {
  constructor(private http:Http,private httpClient: HttpClient,
    private auth: OauthService) 
    { }
    getMachineList(id:string,pageNumber:number,size:number){
      return this.http.get(environment.servicesURL
        +'/api/v1/admin/machines/'
        +id+'?page='+pageNumber+'&size='+size,this.auth.options);
    }
    getMachineById(id:string){
      return this.http.get(environment.servicesURL
        +'api/v1/machine/'+id,this.auth.options);
    }
    getType(){
      return this.http.get(environment.servicesURL
        +'/api/v1/machinetypes/all',this.auth.options);
    }
    getMake(selectedTypeId:number){
      return this.http.get(environment.servicesURL
        +'api/v1/makes/'+selectedTypeId,this.auth.options);
    }
    getModel(selectedMakeId:number){
      return this.http.get(environment.servicesURL
        +'/api/v1/models/'+selectedMakeId,this.auth.options);
    }
    updateMachine(machine:Machine){
      return this.http.put(environment.servicesURL
        +'/api/v2/machine',machine,this.auth.options);
    }
    savedMachine(machine:Machine){
      return this.http.post(environment.servicesURL
        +'api/v2/machine',machine,this.auth.options);
    }
    getMachineStatusList(id:string,searchText:string,pageNumber:number,size:number){
      return this.http.get(environment.servicesURL
        +'api/v2/machines/status/'+id+'?searchText='
        +searchText+'&page='+pageNumber+'&size='+size,this.auth.options);
    }
}
