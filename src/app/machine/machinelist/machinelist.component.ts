import { Component, OnInit, ElementRef } from '@angular/core';
import { MachineService } from '../machine.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Machine } from 'src/app/model/machine.model';
import { ColumnMode } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-machinelist',
  templateUrl: './machinelist.component.html',
  styleUrls: ['./machinelist.component.scss']
})
export class MachinelistComponent implements OnInit {

  public userId:any;
  readonly headerHeight = 50;
  readonly rowHeight = 50;
  readonly pageLimit = 1;
  isLoading: boolean;
  rows: Machine[] = [];
  ColumnMode = ColumnMode;
  constructor(public router:Router,public machineservice:MachineService,
    private route: ActivatedRoute,private toastr: ToastrService,
    private el: ElementRef) { }

  ngOnInit() {
       
    this.route.params.forEach((params: Params) => {
      let id= params['id']; 
      this.userId = params['id']; 
      console.log("user",this.userId);
    });
    this.onScroll(0);
  }

  onScroll(offsetY: number) {
    
    // total height of all rows in the viewport
    const viewHeight = this.el.nativeElement.getBoundingClientRect().height
     - this.headerHeight;
     console.log("viewheight",this.el.nativeElement.getBoundingClientRect().height);
    //check if we scrolled to the end of the viewport
    if (!this.isLoading && offsetY + viewHeight >= this.rows.length * this.rowHeight) {
      // total number of results to load
      let limit = this.pageLimit;
      // check if we haven't fetched any results yet
      if (this.rows.length === 0) {
        // calculate the number of rows that fit within viewport
        const pageSize = Math.ceil(viewHeight / this.rowHeight);
        // change the limit to pageSize such that we fill the first page entirely
        // (otherwise, we won't be able to scroll past it)
        limit = Math.max(pageSize, this.pageLimit);
      }
      
        this.loadMachinePage(limit);
    }
  }

  private loadMachinePage(limit: number) {
    // set the loading flag, which serves two purposes:
    // 1) it prevents the same page from being loaded twice
    // 2) it enables display of the loading indicator
    this.isLoading = true;
    this.machineservice.getMachineList(this.userId,this.rows.length, limit)
    .subscribe(response => {
      const rows = [...this.rows, ...response.json().content];
      console.log("machine",rows);
      this.rows = rows;
      this.isLoading = false;
    });
  }

}
