import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MachineService } from '../machine.service';
import { ToastrService } from 'ngx-toastr';
import { CommonserviceService } from 'src/app/commonservice/commonservice.service';
import { Year } from 'src/app/model/year.model';
import { MachineType } from 'src/app/model/machinetype.model';
import { Make } from 'src/app/model/make.model';
import { Model } from 'src/app/model/model.model';
import { Machine } from 'src/app/model/machine.model';
import { User } from 'src/app/model/users.model';
declare var jQuery:any;

@Component({
  selector: 'app-addmachine',
  templateUrl: './addmachine.component.html',
  styleUrls: ['./addmachine.component.scss']
})
export class AddmachineComponent implements OnInit {
  public userId : any;
  public year:Year[]= new Array();
  public type:MachineType[]=new Array();
  public selectedMakeId:number=0;
  public selectedModelId:number=0;
  public machineTypeId:number;
  public selctedType:any=0;
  public selectedTypeId: number;
  public make:Make[]=new Array();
  public model:Model[]=new Array();
  public isBreaker:boolean;
  public machine= new Machine();
  public selectedYearId:Year=null;

  constructor(public router:Router,public machineService:MachineService,
    private toastr: ToastrService,private route: ActivatedRoute,
    public commonService:CommonserviceService) { }

  ngOnInit() {
    this.getYear();
    this.getType();
    this.route.params.forEach((params: Params) => {
      let addmachine= params['addmachine']; 
      this.userId = params['id']; 
      });
  }

  getYear(){
    this.commonService.getYear().subscribe(
    (response:any) => {
    this.year = response.json();
    },
    (error) => {
    console.log(error);
        }
    )
  }
  selectedYear(selectedYearId:Year){
    if(this.selectedYearId!=null){
        this.getYear();
      }
  }
  getType(){
    this.machineService.getType().subscribe(
    (response:any) => {
      this.type = response.json();
      this.selectedMakeId = 0;
      this.selectedModelId = 0;
      },
        (error) => {
        console.log(error);
      }
    ) 
  }
  selectedType(event:any){
    this.machineTypeId=this.selctedType.machineTypeId;
    this.selectedTypeId=this.selctedType.id;
    
    if(this.machineTypeId!=2){
        if(this.selectedTypeId!=0){
          this.getMake(this.selectedTypeId);
          this.model = new Array();
        }
    }
  } 
  getMake(selectedTypeId:number){
    this.machineService.getMake(this.selectedTypeId).subscribe(
      (response:any) => {
          this.make = response.json();
          this.selectedModelId = 0;
        },
        (error) => {
        console.log(error);
        }
    )
  }
  selectedMake(selectedMakeId:number){
    if(this.selectedMakeId!=0){
        this.getModel(this.selectedMakeId);
       }
  }
  getModel(selectedMakeId){
    this.machineService.getModel(this.selectedMakeId).subscribe(
      (response:any) => {
      this.model = response.json();
      },
      (error) => {
      console.log(error);
      this.model = new Array();
      }
    )
  }
  checkValue(){
    if(this.isBreaker==true){
        this.machine.isBreaker="Y";
    }else{
           this.machine.isBreaker="N"; 
    }
   }
   isDataValid(){
    if(!this.machine.vehicleNo||!this.machine.engineNo||this.selctedType==0||
      this.selectedYearId==null||this.selectedTypeId==0||(this.machineTypeId==1 
        && (this.selectedMakeId==0||this.selectedModelId==0))) {
      this.showError("Enter valid details");
      if(!this.machine.vehicleNo)jQuery("#vehicleNo").addClass("mandatory");  
      if(!this.machine.engineNo)jQuery("#engineNo").addClass("mandatory");   
      if(this.selectedYearId==null)jQuery("#yearId").addClass("mandatory");  
      if(this.selectedTypeId==0)jQuery("#typeId").addClass("mandatory");  
      if(this.selctedType==0)jQuery("#typeId").addClass("mandatory");   
      if(this.machineTypeId!=2){
        if(this.selectedMakeId==0)jQuery("#makeId").addClass("mandatory");   
        if(this.selectedModelId==0)jQuery("#modelId").addClass("mandatory"); 
      }
    return false;
    }
    return true;
  }
  removeMandatory(event:any){
    jQuery("#"+event.currentTarget.id).removeClass("mandatory");   
  }
  showError(msg:string) {
    this.toastr.error( msg, 'Error');
  }
  showSuccess(msg:string) {
    this.toastr.success('Machine added succefully', 'Congrats'); 
    //this.router.navigateByUrl('home');
  }
  savedMachine(){
    if(!this.isDataValid()){
    return;
    } 
   /*  this.isLoading = true;
        if(this.insuranceImagUpload.files.length>0){

            this.insuranceImagUpload.upload();

        } else if(this.invoiceImagUpload.files.length>0){

           this.invoiceImagUpload.upload();

        } else{ */

           this.saveMachine();
      /*   } */
  }

  saveMachine(){
    
    this.checkValue();
    this.machine.modelId=this.selectedModelId;
    this.machine.yearId=this.selectedYearId;
    this.machine.capacity;
    this.machine.user=new User();
    this.machine.type=new MachineType();
    this.machine.type.machineTypeId=this.machineTypeId;
    this.machine.user.id=this.userId;
    this.machine.type.id=this.selectedTypeId;
    console.log("this.machine",this.selectedTypeId)
          this.machineService.savedMachine(this.machine).subscribe(
            (response:any) => {
                this.showSuccess("Machine Added succefully");
                //this.isLoading = false;
              /*   this.router.navigateByUrl('addmachine/'+this.userId);
                this.toastr.success('Machine added succefully', 'Congrats'); */
            },
              (error) => {
              console.log(error);
              this.toastr.error('Some error while adding the machine','oops!!!'); 
              //this.isLoading = false;
              }
            )
  }

  cancel(){
     this.router.navigateByUrl('admin/user');
   }

}
