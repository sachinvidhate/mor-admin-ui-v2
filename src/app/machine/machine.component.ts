import { Component, OnInit, ElementRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MachineService } from './machine.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Machine } from '../model/machine.model';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { MachineTypeEnum } from '../enum/machinetypeenum.enum';

@Component({
  selector: 'app-machine',
  templateUrl: './machine.component.html',
  styleUrls: ['./machine.component.scss']
})
export class MachineComponent implements OnInit {
  public id:any;
  public searchText:string=""; 
  readonly headerHeight = 50;
  readonly rowHeight = 50;
  readonly pageLimit = 1;
  isLoading: boolean;
  rows: Machine[] = [];
  ColumnMode = ColumnMode;
  constructor(public router:Router,public machineService:MachineService,
    private route: ActivatedRoute,private toastr: ToastrService,
    private el: ElementRef) { }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      this.id= params['id'];  
    });
    this.onScroll(0);
  }
  onScroll(offsetY: number) {
    // total height of all rows in the viewport
    const viewHeight = this.el.nativeElement.getBoundingClientRect().height
     - this.headerHeight;
     console.log("viewheight",this.el.nativeElement.getBoundingClientRect().height);
    //check if we scrolled to the end of the viewport
    if (!this.isLoading && offsetY + viewHeight >= this.rows.length * this.rowHeight) {
      // total number of results to load
      let limit = this.pageLimit;
      // check if we haven't fetched any results yet
      if (this.rows.length === 0) {
        // calculate the number of rows that fit within viewport
        const pageSize = Math.ceil(viewHeight / this.rowHeight);
        // change the limit to pageSize such that we fill the first page entirely
        // (otherwise, we won't be able to scroll past it)
        limit = Math.max(pageSize, this.pageLimit);
      }
     /*  console.log("id",this.id);
      if(this.role == "owner"){
        this.loadOwnerPage(limit);
      }else{
        this.loadUserPage(limit);
      } */
      if(this.id==MachineTypeEnum.DOC_PENDING){
        this.getDocPendingMachineList(limit);
      }else if(this.id=MachineTypeEnum.PENDING_GPS){
        this.getPendingForGpsActivationMachineList(limit);
      }else if(this.id=MachineTypeEnum.RATE_PENDING){
        this.getRatesPendingMachineList(limit);
      }else if(this.id=MachineTypeEnum.IDLE){
        this.getIdleMachineList
      }else{
        this.getOnRentMachineList(limit);
      }
     
      
    }
  }
  private getDocPendingMachineList(limit: number) {
    // set the loading flag, which serves two purposes:
    // 1) it prevents the same page from being loaded twice
    // 2) it enables display of the loading indicator
    console.log("owner page");
    this.isLoading = true;
    this.machineService.getMachineStatusList(this.id,this.searchText,this.rows.length, limit)
    .subscribe(response => {
      const rows = [...this.rows, ...response.json().content];
      console.log("machine content",rows);
      this.rows = rows;
      this.isLoading = false;
    });
  }
  private getPendingForGpsActivationMachineList(limit: number) {
    // set the loading flag, which serves two purposes:
    // 1) it prevents the same page from being loaded twice
    // 2) it enables display of the loading indicator
    console.log("owner page");
    this.isLoading = true;
    this.machineService.getMachineStatusList(this.id,this.searchText,this.rows.length, limit)
    .subscribe(response => {
      const rows = [...this.rows, ...response.json().content];
      console.log("machine content",rows);
      this.rows = rows;
      this.isLoading = false;
    });
  }
  private getRatesPendingMachineList(limit: number) {
    // set the loading flag, which serves two purposes:
    // 1) it prevents the same page from being loaded twice
    // 2) it enables display of the loading indicator
    console.log("owner page");
    this.isLoading = true;
    this.machineService.getMachineStatusList(this.id,this.searchText,this.rows.length, limit)
    .subscribe(response => {
      const rows = [...this.rows, ...response.json().content];
      console.log("machine content",rows);
      this.rows = rows;
      this.isLoading = false;
    });
  }
  private getOnRentMachineList(limit: number) {
    // set the loading flag, which serves two purposes:
    // 1) it prevents the same page from being loaded twice
    // 2) it enables display of the loading indicator
    console.log("owner page");
    this.isLoading = true;
    this.machineService.getMachineStatusList(this.id,this.searchText,this.rows.length, limit)
    .subscribe(response => {
      const rows = [...this.rows, ...response.json().content];
      console.log("machine content",rows);
      this.rows = rows;
      this.isLoading = false;
    });
  }
  private getIdleMachineList(limit: number) {
    // set the loading flag, which serves two purposes:
    // 1) it prevents the same page from being loaded twice
    // 2) it enables display of the loading indicator
    console.log("owner page");
    this.isLoading = true;
    this.machineService.getMachineStatusList(this.id,this.searchText,this.rows.length, limit)
    .subscribe(response => {
      const rows = [...this.rows, ...response.json().content];
      console.log("machine content",rows);
      this.rows = rows;
      this.isLoading = false;
    });
  }
  

}
