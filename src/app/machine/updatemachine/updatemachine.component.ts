import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MachineService } from '../machine.service';
import { ToastrService } from 'ngx-toastr';
import { Machine } from 'src/app/model/machine.model';
import { MachinesEnum } from 'src/app/enum/machines.enum';
import { Year } from 'src/app/model/year.model';
import { CommonserviceService } from 'src/app/commonservice/commonservice.service';
import { MachineType } from 'src/app/model/machinetype.model';
import { Make } from 'src/app/model/make.model';
import { Model } from 'src/app/model/model.model';
import { User } from 'src/app/model/users.model';
import { environment } from 'src/environments/environment';
declare var jQuery:any;

@Component({
  selector: 'app-updatemachine',
  templateUrl: './updatemachine.component.html',
  styleUrls: ['./updatemachine.component.scss']
})
export class UpdatemachineComponent implements OnInit {
  public userId : any;
  public machine = new Machine();
  public machineTypeId : any;
  public selectedMakeId:number=0;
  public selectedModelId:number=0;
  public selectedTypeId:number=0;
  public selectedYearId:Year=null;
  public year:Year[]= new Array();
  public type:MachineType[]=new Array();
  public make:Make[]=new Array();
  public model:Model[]=new Array();
  public checkBreaker:string;
  public isBreaker:boolean;
  public url:string= environment.servicesURL + "image/upload/multi"
  public downloadImageUrl:string=environment.servicesURL + "/api/v1/file/download/"
/*   @ViewChild('insuranceImagUpload') insuranceImagUpload: FileUpload;
  @ViewChild('invoiceImagUpload') invoiceImagUpload: FileUpload; */

  constructor(public router:Router,public machineService : MachineService,
    private route: ActivatedRoute,private toastr: ToastrService,
    public commonService : CommonserviceService) { }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      let id = params['id']; 
      this.userId=params['userId'];
      this.getMachineById(id); 
    });
  }

  getMachineById(id:string){
    this.machineService.getMachineById(id).subscribe(
        (response:any) => {
          this.machine = response.json();
          console.log("updatmachine",this.machine);
          if(response.json().machinetype==null){
          this.machineTypeId=MachinesEnum.CONSTRUCTION_MACHINE;
          this.selectedTypeId=response.json().model.make.type.id;
          this.selectedMakeId=response.json().model.make.id;
          this.selectedModelId=response.json().model.id;
          }else{
            this.machineTypeId=MachinesEnum.OTHER_MACHINE;
            this.selectedTypeId=response.json().machinetype.id;
          }
          this.selectedYearId=response.json().year.id;
        /*   this.insuranceImagUploadId=response.json().insuranceImgId;
          this.invoiceImagUploadId=response.json().invoiceImgId; */
          this.getYear();
          this.getType();
          this.getMake(this.selectedTypeId,this.selectedMakeId);  
          this.getModel(this.selectedMakeId,this.selectedModelId);
          this.checkBreaker=response.json().isBreaker; 
                  if(this.checkBreaker.toLowerCase() == "y"){
                              this.isBreaker=true;
                            } else {
                              this.isBreaker=false; 
                            }

                        /*   this.getImage(this.insuranceImagUploadId,this.file);  
                          this.getImage(this.invoiceImagUploadId,this.invoiceFile); */
                    },
        (error) => {
          console.log(error);
        }
    )
  }
  getYear(){
    this.commonService.getYear().subscribe(
      (response:any) => {
      this.year = response.json();
      },
      (error) => {
      console.log(error);

      }
    )
  }
  getType(){
    this.machineService.getType().subscribe(
      (response:any) => {
        this.type = response.json();
      },
      (error) => {
        console.log(error);
      }
    )
  }
  getMake(selectedTypeId:number,selectedMakeId:number){
    this.machineService.getMake(selectedTypeId).subscribe(
        (response:any) => {
          this.make = response.json();
          if(selectedMakeId!=0){
            this.selectedMakeId=selectedMakeId;
          }else{
              this.selectedMakeId=0;
              this.selectedModelId=0;
            }
        },
        (error) => {
          console.log(error);
        }
    )

  }
  getModel(selectedMakeId,selectedModelId:number){
    this.machineService.getModel(selectedMakeId).subscribe(
      (response:any) => {
        this.model = response.json();
        if(selectedModelId!=0){
          this.selectedModelId=selectedModelId;
          }else{
            this.selectedModelId=0;
          }
        },
      (error) => {
      console.log(error);
      this.model = new Array();
      }
    )
  } 
  updateMachine(){
    if(!this.isDataValid()){
    return;
    } /* 
      if(this.insuranceImagUpload.files.length>0){

          this.insuranceImagUpload.upload();

      } else if(this.invoiceImagUpload.files.length>0){

         this.invoiceImagUpload.upload();

      } else{ */

         this.editMachine();
     /*  } */
      } 
  editMachine(){
    if(!this.isDataValid()){
    return;
    } 
    this.checkValue();
    this.machine.modelId=this.selectedModelId;
    this.machine.yearId=this.selectedYearId;
    /*  this.machine.capacityId=this.selectedCapacityId; */
    this.machine.capacity;
    this.machine.user=new User();
    this.machine.user.id=this.userId;
    this.machine.tncAgreed=true;
    this.machine.type=new MachineType();
    this.machine.type.machineTypeId=this.machineTypeId;
    this.machineService.updateMachine(this.machine).subscribe(
          (response:any) => {
            this.showSuccess("Machine Updated Succefully"); 
          },
          (error) => {
          console.log(error);
          this.toastr.error('Some error while updating the machine','oops!!!'); 
        }
    )
  }
  checkValue(){
    if(this.isBreaker==true){
      this.machine.isBreaker="Y";
    }else{
      this.machine.isBreaker="N"; 
    }

  }

  isDataValid(){
    if(!this.machine.vehicleNo||!this.machine.engineNo||
      this.selectedYearId==null||this.selectedTypeId==0
      ||(this.machineTypeId==1 && (this.selectedMakeId==0||this.selectedModelId==0))) {
      this.showError("Enter valid details");
      if(!this.machine.vehicleNo)jQuery("#vehicleNo").addClass("mandatory"); 
      if(!this.machine.engineNo)jQuery("#engineNo").addClass("mandatory");  
      if(this.selectedYearId==null)jQuery("#yearId").addClass("mandatory");  
      if(this.selectedTypeId==0)jQuery("#typeId").addClass("mandatory");  
      if(this.machineTypeId!=2){ 
          if(this.selectedMakeId==0)jQuery("#makeId").addClass("mandatory");   
          if(this.selectedModelId==0)jQuery("#modelId").addClass("mandatory"); 
      }
    return false;
    }
    return true;
  }
  removeMandatory(event:any){
    jQuery("#"+event.currentTarget.id).removeClass("mandatory");
  }
  showError(msg:string) {
    this.toastr.error( msg, 'Error');
  }
  showSuccess(msg:string) {
   this.toastr.success('Machine updated succefully', 'Congrats');
  }
  cancel(){
    this.router.navigateByUrl('machine/'+this.userId);
  }
}
