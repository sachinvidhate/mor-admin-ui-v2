import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MachineComponent } from './machine.component';
import { RouterModule, Routes } from '@angular/router';
import { AddmachineComponent } from './addmachine/addmachine.component';
import { UpdatemachineComponent } from './updatemachine/updatemachine.component';
import { MachinelistComponent } from './machinelist/machinelist.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'machinestatus/:id',
        component:MachineComponent
      },
      {
        path: 'addmachine/:id',
        component:AddmachineComponent
      },
      {
      path: 'machinelist/:id',
      component:MachinelistComponent
      },
      {
        path: 'updatemachine/:userId/:id',
        component:UpdatemachineComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MachineRoutingModule { }
