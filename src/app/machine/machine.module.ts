import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MachineComponent } from './machine.component';
import { MachineRoutingModule } from './machine-routing.module';
import { AddmachineComponent } from './addmachine/addmachine.component';
import { UpdatemachineComponent } from './updatemachine/updatemachine.component';
import { MachinelistComponent } from './machinelist/machinelist.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CardModule } from '../theme/shared/components';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    MachineComponent,
    AddmachineComponent,
    UpdatemachineComponent,
    MachinelistComponent
  ],
  imports: [
    CommonModule,
    MachineRoutingModule,
    NgxDatatableModule,
    CardModule,
    FormsModule
  ]
})
export class MachineModule { }
