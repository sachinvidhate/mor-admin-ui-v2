import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, RequestOptions,Headers} from '@angular/http';
import { environment } from 'src/environments/environment';
import { Cookie } from 'ng2-cookies';


export class Foo {
  constructor(
    public id: number,
    public name: string) { }
} 

@Injectable()
export class OauthService {
  public options:any;
  constructor(
    private _router: Router, private _http: Http){}

    obtainAccessToken(uname,pass){
   
      let params = new URLSearchParams();
      params.append('username',uname);
      params.append('password',pass);    
      params.append('grant_type',"password");
      params.append('client_id','morclientid');
      let headers = new Headers({'Content-type': 
      'application/x-www-form-urlencoded; charset=utf-8',
       'Authorization': 'Basic '+btoa("morclientid:AB8kmzoNzl100")});
      let options = new RequestOptions({headers:headers});
      return this._http.post(environment.oAuthTokenUrl+'oauth/token', 
      params.toString(), options);
    }

    saveToken(token){
      var expireDate = new Date().getTime() + (1000 * token.expires_in);
      Cookie.set("access_token", token.access_token, expireDate);
      var headers = new Headers({ 'Authorization': 'Bearer '
      +Cookie.get('access_token')});
      this.options = new RequestOptions({ headers: headers });
      console.log("this.options: ",this.options)
      localStorage.setItem('options',JSON.stringify(this.options));
    }

    checkCredentials(){
      if (!Cookie.check('access_token')){
          this._router.navigate(['/']);
      }
    } 
    checkAccessToken(){
      return Cookie.check('access_token');
    }
    logout() {
      sessionStorage.clear();
      localStorage.removeItem('earlierURL');
      Cookie.delete('access_token');
      this._router.navigate(['/']);
    }
    
}
