import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  loggedIn=false;
  constructor(private http:Http) { }
  logout(){
   
      sessionStorage.clear();
      this.loggedIn=false;
  }
}
