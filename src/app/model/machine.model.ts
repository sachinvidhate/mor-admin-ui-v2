import { User } from './users.model';
import { Year } from './year.model';
import { MachineType } from './machinetype.model';


export class Machine {

    id:number;
	uniqeMachineId:String;
	type:MachineType;
    createdDateTime:Date;
	updatedDateTime:Date;
	vehicleNo:string;
	engineNo:string;
	modelId:number;
	isBreaker:string;
	ownerName:string;
	capacity:number;
	yearId:Year;
	insuranceImgId:number;
	invoiceImgId:number;
	longitude:string;
	latitude:string;
	currentMeter:DoubleRange;
	description:string;
	machineStatus:number;
	tncAgreed:boolean=true;
	user:User;
	insuranceImageName:String;
	invoiceImageName:String;
	constructor(){
        
    }

}

