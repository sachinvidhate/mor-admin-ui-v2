import { BankAccount } from './bankaccount.model';


export class User {

	id:number;
	fullName:String;
	emailId:String;
	createdDateTime:Date;
	lastLogInDateTime:Date;
	updatedDateTime:Date;
	address1:String;
	address2:String;
	city:String;	
	mobile:String;
	entity:String
	gst:String;
	pan:String;
	pin:String;
	state:String;
	accounts:BankAccount;
	panImageId:number;
	accName:String;
    accNo:String;
    ifsc:String;
    branch:String;
	upi:String;
	password:string;
	comments:string;
    constructor(){
        
    }

}

