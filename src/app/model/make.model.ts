import { MachineType } from "./machinetype.model";

export class Make {

    id:number;
	name:string;
	type:MachineType;
   	description:string;
	status:string;
	
    constructor(){
        
    }

}

