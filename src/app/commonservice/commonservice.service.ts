import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { OauthService } from '../service/oauth.service';
import { environment } from '../../environments/environment'; 
@Injectable({
  providedIn: 'root'
})
export class CommonserviceService {

  constructor(private http:Http,private httpClient: HttpClient,
    private auth: OauthService) { }

  getYear(){
    return this.http.get(environment.servicesURL
      +'api/v1/model/years',this.auth.options);
  }
  getCapacities(selectedModelId:number){
    return this.http.get(environment.servicesURL
      +'/api/v2/model/capacities/'+selectedModelId,this.auth.options);
  }
}
